package kpi.configuration;

/**
 * Created by chandra on 10/14/14.
 */
public final class Constants {

    private Constants() {
    }

    public static final float NEW_WEIGHT = 80;
    public static final float RO_WEIGHT = 20;
    public static final float PERCENTAGE_DIVIDER = 100;
    public static final float VAR_TARGET_ADJ = 1;

}
