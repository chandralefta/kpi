package kpi.configuration.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemProcessListener;

/**
 * Created by chandra on 10/30/14.
 */
public class LogProcessListener implements ItemProcessListener<Object, Object> {

    Logger LOGGER = LoggerFactory.getLogger(LogProcessListener.class);

    public void afterProcess(Object item, Object result) {
        if(item!=null) LOGGER.info("Input to Processor: " + item.toString());
        if(result!=null) LOGGER.info("Output of Processor: " + result.toString());
    }

    public void beforeProcess(Object item) {
    }

    public void onProcessError(Object item, Exception e) {
    }
}
