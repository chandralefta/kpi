package kpi.entity;

import kpi.entity.Product;
import kpi.entity.Var;
import kpi.entity.VarType;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by chandra on 10/22/14.
 */

@StaticMetamodel(Var.class)
public class Var_ {
    public static volatile SingularAttribute<Var, Integer> id;
    public static volatile SingularAttribute<Var, String> name;
    public static volatile SingularAttribute<Var, String> description;
    public static volatile SingularAttribute<Var, Float> weight;
    public static volatile SingularAttribute<Var, Product> product;
    public static volatile SingularAttribute<Var, VarType> varType;
}
