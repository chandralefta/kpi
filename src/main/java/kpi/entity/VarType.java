package kpi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by chandra on 10/10/14.
 */

@Entity
@Table(name="var_type")
public class VarType implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "name", nullable=false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "kpi_var_id")
    //@JsonBackReference("kpi_var-var_type")
    private KpiVar kpiVar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KpiVar getKpiVar() {
        return kpiVar;
    }

    public void setKpiVar(KpiVar kpiVar) {
        this.kpiVar = kpiVar;
    }
}
