package kpi.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by chandra on 10/10/14.
 */

@Entity
@Table(name="period")
public class Period implements Serializable{

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "month", nullable=false)
    private Integer month;

    @Column(name = "year", nullable=false)
    private Integer year;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
