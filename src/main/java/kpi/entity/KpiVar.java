package kpi.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by chandra on 10/10/14.
 */


@Entity
@Table(name="kpi_var")
public class KpiVar implements Serializable{

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "name", nullable=false)
    private String name;

    @Column(name = "weight", nullable=false)
    private Float weight;

    @Column(name = "version", nullable=false)
    private Integer version;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
