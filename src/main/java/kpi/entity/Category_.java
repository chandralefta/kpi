package kpi.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by chandra on 10/24/14.
 */
@StaticMetamodel(Category.class)
public class Category_ {
    public static volatile SingularAttribute<Category, Integer> id;
    public static volatile SingularAttribute<Category, String> name;
}