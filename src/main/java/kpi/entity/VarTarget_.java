package kpi.entity;

import kpi.entity.Branch;
import kpi.entity.Period;
import kpi.entity.Var;
import kpi.entity.VarTarget;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by chandra on 10/22/14.
 */
@StaticMetamodel(VarTarget.class)
public class VarTarget_ {
    public static volatile SingularAttribute<VarTarget, Integer> id;
    public static volatile SingularAttribute<VarTarget, Var> var;
    public static volatile SingularAttribute<VarTarget, Branch> branch;
    public static volatile SingularAttribute<VarTarget, Period> period;
    public static volatile SingularAttribute<VarTarget, java.sql.Date> entryDate;
    public static volatile SingularAttribute<VarTarget, Float> target;
    public static volatile SingularAttribute<VarTarget, Integer> validatedBy;
    public static volatile SingularAttribute<VarTarget, java.sql.Timestamp> validatedDate;
}
