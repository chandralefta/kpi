package kpi.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by chandra on 10/24/14.
 */
@StaticMetamodel(KpiVar.class)
public class KpiVar_ {
    public static volatile SingularAttribute<KpiVar, Integer> id;
    public static volatile SingularAttribute<KpiVar, String> name;
    public static volatile SingularAttribute<KpiVar, Float> weight;
    public static volatile SingularAttribute<KpiVar, Integer> version;
}