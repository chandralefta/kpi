package kpi.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by chandra on 10/10/14.
 */

@Entity
@Table(name="var_value")
public class VarValue implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "var_id")
    private Var var;

    @ManyToOne
    @JoinColumn(name = "period_id")
    private Period period;

    @Column(name = "value", nullable=false)
    private Float value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }
}
