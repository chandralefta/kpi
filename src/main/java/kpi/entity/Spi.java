package kpi.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity 
@Table(name="spi")
public class Spi implements Serializable {

	@Id
	@GeneratedValue
	private int id;

    @ManyToOne
    @JoinColumn(name = "var_target_id")
    private VarTarget varTarget;

	@ManyToOne
	@JoinColumn(name = "var_achievement_id")
	private VarAchievement varAchievement;

    @ManyToOne
    @JoinColumn(name = "branch_id")
    private Branch branch;

    @ManyToOne
    @JoinColumn(name = "kpi_var_id")
    private KpiVar kpiVar;

    @Column(name = "periode_date", nullable = false)
    private java.sql.Date periodeDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VarTarget getVarTarget() {
        return varTarget;
    }

    public void setVarTarget(VarTarget varTarget) {
        this.varTarget = varTarget;
    }

    public VarAchievement getVarAchievement() {
        return varAchievement;
    }

    public void setVarAchievement(VarAchievement varAchievement) {
        this.varAchievement = varAchievement;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Date getPeriodeDate() {
        return periodeDate;
    }

    public void setPeriodeDate(Date periodeDate) {
        this.periodeDate = periodeDate;
    }

    public KpiVar getKpiVar() {
        return kpiVar;
    }

    public void setKpiVar(KpiVar kpiVar) {
        this.kpiVar = kpiVar;
    }
}
