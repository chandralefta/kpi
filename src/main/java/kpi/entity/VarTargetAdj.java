package kpi.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by chandra on 10/21/14.
 */

@Entity
@Table(name="var_target_adj")
public class VarTargetAdj implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "period_id")
    private Period period;

    @ManyToOne
    @JoinColumn(name = "var_target_id")
    private VarTarget varTarget;

    @Column(name = "adjustment", nullable = false)
    private Float adjustment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public VarTarget getVarTarget() {
        return varTarget;
    }

    public void setVarTarget(VarTarget varTarget) {
        this.varTarget = varTarget;
    }

    public Float getAdjustment() {
        return adjustment;
    }

    public void setAdjustment(Float adjustment) {
        this.adjustment = adjustment;
    }
}
