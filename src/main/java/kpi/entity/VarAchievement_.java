package kpi.entity;

import kpi.entity.Branch;
import kpi.entity.Var;
import kpi.entity.VarAchievement;
import kpi.entity.VarTarget;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by chandra on 10/22/14.
 */

@StaticMetamodel(VarAchievement.class)
public class VarAchievement_ {
    public static volatile SingularAttribute<VarAchievement, Integer> id;
    public static volatile SingularAttribute<VarAchievement, Var> var;
    public static volatile SingularAttribute<VarAchievement, Branch> branch;
    public static volatile SingularAttribute<VarAchievement, VarTarget> varTarget;
    public static volatile SingularAttribute<VarAchievement, java.sql.Date> entryDate;
    public static volatile SingularAttribute<VarAchievement, Float> achievement;
    public static volatile SingularAttribute<VarAchievement, Integer> validatedBy;
    public static volatile SingularAttribute<VarAchievement, java.sql.Timestamp> validatedDate;
}
