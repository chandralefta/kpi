package kpi.entity;

import kpi.entity.Branch;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by chandra on 10/22/14.
 */
@StaticMetamodel(Branch.class)
public class Branch_ {
    public static volatile SingularAttribute<Branch, Integer> id;
    public static volatile SingularAttribute<Branch, String> name;
    public static volatile SingularAttribute<Branch, String> description;
}
