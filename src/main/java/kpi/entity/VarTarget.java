package kpi.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by chandra on 10/10/14.
 */

@Entity
@Table(name="var_target")
public class VarTarget implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "var_id")
    private Var var;

    @ManyToOne
    @JoinColumn(name = "branch_id")
    private Branch branch;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "period_id")
    private Period period;

    @Column(name = "entry_date", nullable = false)
    private java.sql.Date entryDate;

    @Column(name = "target", nullable = false)
    private Float target;

    @Column(name = "validated_by", nullable = false)
    private Integer validatedBy;

    @Column(name = "validated_date", nullable = false)
    private java.sql.Timestamp validatedDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public Float getTarget() {
        return target;
    }

    public void setTarget(Float target) {
        this.target = target;
    }

    public Integer getValidatedBy() {
        return validatedBy;
    }

    public void setValidatedBy(Integer validatedBy) {
        this.validatedBy = validatedBy;
    }

    public Timestamp getValidatedDate() {
        return validatedDate;
    }

    public void setValidatedDate(Timestamp validatedDate) {
        this.validatedDate = validatedDate;
    }
}
