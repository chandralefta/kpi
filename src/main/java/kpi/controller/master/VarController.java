package kpi.controller.master;

import kpi.entity.Var;
import kpi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by chandra on 10/24/14.
 */
@RestController
public class VarController {

    @Autowired
    private VarService varService;

    /**
     * CRUD VAR
     */

    @RequestMapping(value="/var", method= RequestMethod.GET)
    @ResponseBody
    public List<Var> findAllKpiVarType(
            Pageable pageable,
            HttpServletResponse response) {
        List<Var> hasil = varService.findAll(pageable).getContent();

        return hasil;
    }

    @RequestMapping(value="/var", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createVar(@RequestBody Var var){
        varService.save(var);
    }

    @RequestMapping(value="/var/{id}", method=RequestMethod.GET)
    public Var varFindById(@PathVariable Integer id){
        return varService.findVarById(id);
    }

    @RequestMapping(value="/var/{id}", method=RequestMethod.PUT)
    public void updateVar(@PathVariable Integer id, @RequestBody Var var){
        varService.update(id, var);
    }

    @RequestMapping(value="/var/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteVar(@PathVariable Integer id){
        varService.delete(id);
    }

}
