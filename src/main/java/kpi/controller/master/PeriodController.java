package kpi.controller.master;

import kpi.entity.Period;
import kpi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by chandra on 10/24/14.
 */
@RestController
public class PeriodController {

    @Autowired
    private PeriodService periodService;


    /**
     * CRUD Period
     */

    @RequestMapping(value="/period", method= RequestMethod.GET)
    @ResponseBody
    public List<Period> findAllPeriod(
            Pageable pageable,
            HttpServletResponse response) {
        List<Period> hasil = periodService.findAll(pageable).getContent();

        return hasil;
    }

    @RequestMapping(value="/period", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createPeriod(@RequestBody Period period){
        periodService.save(period);
    }

    @RequestMapping(value="/period/{id}", method=RequestMethod.GET)
    public Period periodFindById(@PathVariable Integer id){
        return periodService.findById(id);
    }

    @RequestMapping(value="/period/{id}", method=RequestMethod.PUT)
    public void updatePeriod(@PathVariable Integer id, @RequestBody Period period){
        periodService.update(id, period);
    }

    @RequestMapping(value="/period/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deletePeriod(@PathVariable Integer id){
        periodService.delete(id);
    }
}
