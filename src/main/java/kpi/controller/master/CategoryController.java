package kpi.controller.master;

import kpi.entity.Category;
import kpi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by chandra on 10/24/14.
 */
@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * CRUD Category
     */

    @RequestMapping(value="/category", method= RequestMethod.GET)
    @ResponseBody
    public List<Category> findAllCategory(
            Pageable pageable,
            HttpServletResponse response) {
        List<Category> hasil = categoryService.findAll(pageable).getContent();

        return hasil;
    }

    @RequestMapping(value="/category", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createCategory(@RequestBody Category category){
        categoryService.save(category);
    }

    @RequestMapping(value="/category/{id}", method=RequestMethod.GET)
    public Category categoryFindById(@PathVariable Integer id){
        return categoryService.findById(id);
    }

    @RequestMapping(value="/category/{id}", method=RequestMethod.PUT)
    public void updateCategory(@PathVariable Integer id, @RequestBody Category category){
        Category categoryOld = categoryService.findById(id);
        if(categoryOld == null){
            throw new IllegalStateException("No category Found");
        }
        category.setId(categoryOld.getId());
        categoryService.save(categoryOld);
    }

    @RequestMapping(value="/category/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteCategory(@PathVariable Integer id){
        categoryService.delete(id);
    }

}
