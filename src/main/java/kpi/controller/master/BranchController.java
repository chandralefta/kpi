package kpi.controller.master;

import kpi.entity.Branch;
import kpi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by chandra on 10/24/14.
 */
@RestController
public class BranchController {

    @Autowired
    private  BranchService branchService;

    /**
     * CRUD Branch
     */

    @RequestMapping(value="/branch", method= RequestMethod.GET)
    @ResponseBody
    public List<Branch> findAllBranch(
            Pageable pageable,
            HttpServletResponse response) {
        List<Branch> hasil = branchService.findAll(pageable).getContent();

        return hasil;
    }

    @RequestMapping(value="/branch", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createBranch(@RequestBody Branch branch){
        branchService.save(branch);
    }

    @RequestMapping(value="/branch/{id}", method=RequestMethod.GET)
    public Branch branchFindById(@PathVariable Integer id){
        return branchService.findById(id);
    }

    @RequestMapping(value="/branch/{id}", method=RequestMethod.PUT)
    public void updateBranch(@PathVariable Integer id, @RequestBody Branch branch){
        branchService.update(id, branch);
    }

    @RequestMapping(value="/branch/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteBranch(@PathVariable Integer id){
        branchService.delete(id);
    }
}
