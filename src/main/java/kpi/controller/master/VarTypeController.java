package kpi.controller.master;

import kpi.entity.VarType;
import kpi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by chandra on 10/24/14.
 */
@RestController
public class VarTypeController {

    @Autowired
    private VarTypeService varTypeService;

    /**
     * CRUD VAR_TYPE
     */

    @RequestMapping(value="/var_type", method= RequestMethod.GET)
    @ResponseBody
    public List<VarType> findAllVarType(
            Pageable pageable,
            HttpServletResponse response) {
        List<VarType> hasil = varTypeService.findAll(pageable).getContent();

        return hasil;
    }

    @RequestMapping(value="/var_type", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createVarType(@RequestBody VarType varType){
        varTypeService.save(varType);
    }

    @RequestMapping(value="/var_type/{id}", method=RequestMethod.GET)
    public VarType kpiVarTypeFindById(@PathVariable Integer id){
        return varTypeService.findVarTypeById(id);
    }

    @RequestMapping(value="/var_type/{id}", method=RequestMethod.PUT)
    public void updateVarType(@PathVariable Integer id, @RequestBody VarType varType){
        VarType varTypeOld = varTypeService.findVarTypeById(id);
        if(varTypeOld == null){
            throw new IllegalStateException("No Var Type Found");
        }
        varType.setId(varTypeOld.getId());
        varTypeService.save(varTypeOld);
    }

    @RequestMapping(value="/var_type/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteVarType(@PathVariable Integer id){
        varTypeService.delete(id);
    }
}
