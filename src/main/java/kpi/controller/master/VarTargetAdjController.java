package kpi.controller.master;

import kpi.entity.VarTargetAdj;
import kpi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by chandra on 10/24/14.
 */
@RestController
public class VarTargetAdjController {

    @Autowired
    private VarTargetAdjService varTargetAdjService;


    /**
     * CRUD Target Adjustment
     */

    @RequestMapping(value="/target_adj", method= RequestMethod.GET)
    @ResponseBody
    public List<VarTargetAdj> findAllTargetAdj(
            Pageable pageable,
            HttpServletResponse response) {
        List<VarTargetAdj> hasil = varTargetAdjService.findAll(pageable).getContent();

        return hasil;
    }

    @RequestMapping(value="/target_adj", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createTargetAdj(@RequestBody VarTargetAdj varTargetAdj){
        varTargetAdjService.save(varTargetAdj);
    }

    @RequestMapping(value="/target_adj/{id}", method=RequestMethod.GET)
    public VarTargetAdj targetAdjFindById(@PathVariable Integer id){
        return varTargetAdjService.findById(id);
    }

    @RequestMapping(value="/target_adj/{id}", method=RequestMethod.PUT)
    public void targetAdjPeriod(@PathVariable Integer id, @RequestBody VarTargetAdj varTargetAdj){
        varTargetAdjService.update(id, varTargetAdj);
    }

    @RequestMapping(value="/target_adj/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteTargetAdj(@PathVariable Integer id){
        varTargetAdjService.delete(id);
    }
}
