package kpi.controller.master;

import kpi.entity.KpiVar;
import kpi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by chandra on 10/24/14.
 */
@RestController
public class KpiVarController {

    @Autowired
    private KpiVarService kpiVarService;

    /**
     * CRUD KPI VAR
     */

    @RequestMapping(value="/kpi_var", method= RequestMethod.GET)
    @ResponseBody
    public List<KpiVar> findAllKpiVar(
            Pageable pageable,
            HttpServletResponse response) {
        List<KpiVar> hasil = kpiVarService.findAll(pageable).getContent();

        return hasil;
    }

    @RequestMapping(value="/kpi_var", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createKpiVar(@RequestBody KpiVar kpiVar){
        kpiVarService.save(kpiVar);
    }

    @RequestMapping(value="/kpi_var/{id}", method=RequestMethod.GET)
    public KpiVar kpiVarFindById(@PathVariable Integer id){
        return kpiVarService.findKpiVarById(id);
    }

    @RequestMapping(value="/kpi_var/{id}", method=RequestMethod.PUT)
    public void updateKpiVar(@PathVariable Integer id, @RequestBody KpiVar kpiVar){
        kpiVarService.update(id, kpiVar);
    }

    @RequestMapping(value="/kpi_var/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteKpiVar(@PathVariable Integer id){
        kpiVarService.delete(id);
    }
}
