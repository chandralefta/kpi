package kpi.controller;

import kpi.entity.Product;
import kpi.entity.VarTarget;
import kpi.service.VarTargetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
public class TargetController {

    @Autowired
    private VarTargetService varTargetService;

    @RequestMapping(value="/target", method= RequestMethod.GET)
    @ResponseBody
    public List<VarTarget> findAll(
            Pageable pageable,
            HttpServletResponse response) {
        List<VarTarget> hasil = varTargetService.findAll(pageable).getContent();
        return hasil;
    }

    @RequestMapping(value="/target", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody VarTarget varTarget){
        varTargetService.save(varTarget);
    }

    @RequestMapping(value="/target/{id}", method=RequestMethod.GET)
    public VarTarget findById(@PathVariable Integer id){
        return varTargetService.findById(id);
    }

    @RequestMapping(value="/target/{id}", method=RequestMethod.PUT)
    public void update(@PathVariable Integer id, @RequestBody VarTarget varTarget){
        varTargetService.update(id, varTarget);
    }

    @RequestMapping(value="/target/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Integer id){
        varTargetService.delete(id);
    }
}