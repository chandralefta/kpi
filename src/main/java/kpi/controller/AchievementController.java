package kpi.controller;

import kpi.entity.VarAchievement;
import kpi.service.VarAchievementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
public class AchievementController {
	
	@Autowired
	private VarAchievementService varAchievementService;

    @RequestMapping(value="/achievement", method= RequestMethod.GET)
    @ResponseBody
    public List<VarAchievement> findAll(
            Pageable pageable,
            HttpServletResponse response) {
        List<VarAchievement> hasil = varAchievementService.findAll(pageable).getContent();
        return hasil;
    }

    @RequestMapping(value="/achievement", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody VarAchievement varAchievement){
        varAchievementService.save(varAchievement);
    }

    @RequestMapping(value="/achievement/{id}", method=RequestMethod.GET)
    public VarAchievement findById(@PathVariable Integer id){
        return varAchievementService.findById(id);
    }

    @RequestMapping(value="/achievement/{id}", method=RequestMethod.PUT)
    public void update(@PathVariable Integer id, @RequestBody VarAchievement varAchievement){
        varAchievementService.update(id, varAchievement);
    }

    @RequestMapping(value="/achievement/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Integer id){
        varAchievementService.delete(id);
    }
}
