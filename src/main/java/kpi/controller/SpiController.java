package kpi.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import kpi.entity.*;
import kpi.repository.ProductRepository;
import kpi.service.ProductService;
import kpi.service.SpiService;
import kpi.service.VarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import kpi.repository.SpiRepository;

import javax.servlet.http.HttpServletResponse;


@RestController
public class SpiController {
	
	@Autowired 
	private ProductService productService;

    @Autowired
    private SpiService spiService;

    @Autowired
    VarService varService;
	
//	@RequestMapping(value="/spi", method=RequestMethod.GET)
//    @ResponseBody
//    public List<VarType> findAll(HttpServletResponse response) {
//        List<VarType> hasil = spiService.fetchSpiData();
//
//        return hasil;
//    }

    @RequestMapping(value="/spi_new", method= RequestMethod.GET)
    @ResponseBody
    public List<Var> findAll(
            Pageable pageable,
            HttpServletResponse response) {
        List<Var> hasil = spiService.fetchDataNew(pageable).getContent();
        return hasil;
    }

    @RequestMapping(value="/spi/{id}", method=RequestMethod.GET)
    public Var varFindById(@PathVariable Integer id){
        return varService.findVarById(id);
    }

    @RequestMapping(value="/spi_period", method=RequestMethod.GET)
    @ResponseBody
    public List<String> period(HttpServletResponse response) {
        String[] monthName = {"January", "February",
                "March", "April", "May", "June", "July",
                "August", "September", "October", "November",
                "December"};

        Calendar cal = Calendar.getInstance();
        String month = monthName[cal.get(Calendar.MONTH)];
        int year = cal.get(Calendar.YEAR);

        List<String> currentPeriod = new ArrayList<String>();
        currentPeriod.add(month);
        currentPeriod.add(new String(String.valueOf(year)));

        return currentPeriod;
    }


}
