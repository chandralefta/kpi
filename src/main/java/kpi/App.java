package kpi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

/**
 * Created by chandra on 10/7/14.
 */

//@Configuration
@ComponentScan
@EnableAutoConfiguration
//@EnableJpaRepositories
//@EnableWebMvc
//@EnableSpringDataWebSupport
@ImportResource("classpath:springScheduler.xml")
public class App {

    public static void main( String[] args ) {

        Logger LOGGER = LoggerFactory.getLogger(App.class);

        SpringApplication.run(App.class);

    }
}
