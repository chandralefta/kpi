package kpi.batch.jobs.varValue;

import kpi.entity.Period;
import kpi.entity.VarAchievement;
import kpi.entity.VarValue;
import kpi.service.VarValueService;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Created by chandra on 10/30/14.
 */
public class CustomItemProcessor implements ItemProcessor<VarAchievement, VarValue>{

    @Autowired
    private VarValueService varValueService;

    @Override
    public VarValue process(VarAchievement varAchievement) throws Exception {

        VarValue varValue = varValueService.calculateVarValue(varAchievement);
        return varValue;
    }
}
