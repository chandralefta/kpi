package kpi.batch.jobs.varValue;

import kpi.configuration.DatabaseConfiguration;
import kpi.configuration.listeners.LogProcessListener;
import kpi.configuration.listeners.ProtocolListener;
import kpi.entity.VarAchievement;
import kpi.entity.VarValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.skip.SkipException;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.activation.DataSource;
import javax.persistence.EntityManagerFactory;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by chandra on 10/30/14.
 */

@Configuration
@EnableBatchProcessing
@Import({DatabaseConfiguration.class})
public class VarValueJobConfiguration {

    @Autowired
    DatabaseConfiguration dataSource;

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;


    @Bean
    public Job addNewPodcastJob(){
        return jobs.get("addNewVarValueJob")
                .listener(protocolListener())
                .start(step())
                .build();
    }

    @Bean
    public Step step(){
        return stepBuilderFactory.get("step")
                .<VarAchievement, VarValue>chunk(1) //important to be one in this case to commit after every line read
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .listener(logProcessListener())
                .faultTolerant()
                .skipLimit(0) //default is set to 0
                .throttleLimit(1)
                .build();
    }

    @Bean
    public ItemReader<VarAchievement> reader(){
        return new CustomItemReader();
    }

    @Bean
    public ItemProcessor<VarAchievement, VarValue> processor() {
        return new CustomItemProcessor();
    }

    @Bean
    public ItemWriter<VarValue> writer() {

        return new CustomItemWriter();
    }


    @Bean
    public ProtocolListener protocolListener(){
        return new ProtocolListener();
    }

    @Bean
    public LogProcessListener logProcessListener(){
        return new LogProcessListener();
    }

}
