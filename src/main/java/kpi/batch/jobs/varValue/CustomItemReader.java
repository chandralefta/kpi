package kpi.batch.jobs.varValue;

import kpi.entity.VarAchievement;
import kpi.repository.VarAchievementRepository;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by chandra on 10/30/14.
 */
public class CustomItemReader implements ItemReader<VarAchievement> {

    @Autowired
    private VarAchievementRepository varAchievementRepository;

    @Override
    public VarAchievement read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

        VarAchievement varAchievement = varAchievementRepository.findOne(15);

        return varAchievement;
    }
}
