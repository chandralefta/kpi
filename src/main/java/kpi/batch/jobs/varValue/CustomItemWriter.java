package kpi.batch.jobs.varValue;


import kpi.entity.VarAchievement;
import kpi.entity.VarTarget;
import kpi.entity.VarValue;
import kpi.repository.VarAchievementRepository;
import kpi.repository.VarTargetRepository;
import kpi.repository.VarValueRepository;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepListener;
import org.springframework.batch.core.listener.ItemListenerSupport;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by chandra on 10/30/14.
 */
public class CustomItemWriter extends ItemListenerSupport implements StepListener, ItemWriter<VarValue> {

    @Autowired
    private CustomItemProcessor processor;

    @Autowired
    private VarTargetRepository varTargetRepository;

    @Autowired
    private VarAchievementRepository varAchievementRepository;

    @Autowired
    private VarValueRepository varValueRepository;

    private StepExecution stepExecution;

    public void beforeStep(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }

    @Override
    public void write(List<? extends VarValue> item) throws Exception {

        for (VarValue varValue : item) {
            varValueRepository.save(varValue);
        }

        afterRead(item);
    }

    public void afterRead(List<? extends VarValue> item) {
        if (item.get(0) == null) {
            stepExecution.setTerminateOnly();
        }
    }

}
