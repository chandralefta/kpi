package kpi.service;

import kpi.entity.Branch;
import kpi.repository.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created by chandra on 10/10/14.
 */

@Service
@Transactional
public class BranchService {

    @Autowired
    private BranchRepository branchRepository;

    public void save(Branch branch) {
        branchRepository.save(branch);
    }

    public void update(Integer id, Branch branch){
        Branch branchOld = branchRepository.findOne(id);
        if(branchOld == null){
            throw new IllegalStateException("No branch found");
        }
        branch.setId(branchOld.getId());
        branchRepository.save(branchOld);
    }

    public void delete(int id) {
        if (id == 0) {
            return;
        }
        branchRepository.delete(id);
    }

    public Branch findById(int id) {
        if (id == 0) {
            return null;
        }
        return branchRepository.findOne(id);
    }

    public Page<Branch> findAll(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return branchRepository.findAll(pageable);
    }

}
