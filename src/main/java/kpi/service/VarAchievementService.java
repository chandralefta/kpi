package kpi.service;

import kpi.entity.VarAchievement;
import kpi.entity.VarTarget;
import kpi.repository.VarAchievementRepository;
import kpi.repository.VarTargetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

import static kpi.repository.VarAchievementSpecification.byEntryDateAndTargetId;
import static kpi.repository.VarAchievementSpecification.getMonthly;


/**
 * Created by chandra on 10/10/14.
 */

@Service
@Transactional
public class VarAchievementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VarAchievementService.class);

    @Autowired
    private VarAchievementRepository varAchievementRepository;

    @Autowired
    private VarTargetRepository varTargetRepository;

    public void save(VarAchievement varAchievement) {

        float sumVarAchivement = 0.0f;

        java.util.Date now = new java.util.Date();

        java.sql.Date entryDate = new java.sql.Date(now.getTime());

        java.sql.Timestamp validateDate = new java.sql.Timestamp(now.getTime());

        Calendar nowDate = Calendar.getInstance();
        int year = nowDate.get(Calendar.YEAR);
        int month = nowDate.get(Calendar.MONTH);

        List<VarAchievement> varAchievementSource = (List<VarAchievement>) varAchievementRepository.findAll(byEntryDateAndTargetId(entryDate, varAchievement.getVarTarget().getId()));
        //System.out.println("==" +varAchievementSource);

        List<VarAchievement> varAchievementValidSum = varAchievementRepository.findAll(getMonthly(month + 1, year, varAchievement.getVarTarget().getId()));
        System.out.println("==" +varAchievementValidSum);

        VarTarget varTarget = varTargetRepository.findOne(varAchievement.getVarTarget().getId());

        try {
            if(varAchievementSource == null || varAchievementSource.isEmpty()) {

                if(varAchievementValidSum == null || varAchievementValidSum.isEmpty()) {
                    if(varAchievement.getAchievement() <= varTarget.getTarget()) {

                        varAchievement.setValidatedDate(validateDate);
                        varAchievement.setValidatedBy(1);
                        varAchievement.setEntryDate(entryDate);
                        varAchievement.setVar(varAchievement.getVar());
                        varAchievement.setBranch(varAchievement.getBranch());
                        varAchievement.setVarTarget(varAchievement.getVarTarget());
                        varAchievement.setAchievement(varAchievement.getAchievement());

                        varAchievementRepository.save(varAchievement);
                    }else{
                        throw new IllegalStateException("Variable achievement melebihi target");
                    }

                }else{
                    for (VarAchievement varAchievementValidSumNew : varAchievementValidSum) {
                        sumVarAchivement += varAchievementValidSumNew.getAchievement() + varAchievement.getAchievement();

                        if(sumVarAchivement <= varTarget.getTarget()){
                            varAchievement.setValidatedDate(validateDate);
                            varAchievement.setValidatedBy(1);
                            varAchievement.setEntryDate(entryDate);
                            varAchievement.setVar(varAchievement.getVar());
                            varAchievement.setBranch(varAchievement.getBranch());
                            varAchievement.setVarTarget(varAchievement.getVarTarget());
                            varAchievement.setAchievement(varAchievement.getAchievement());

                            varAchievementRepository.save(varAchievement);

                        }else{
                            throw new IllegalStateException("Variable achievement melebihi target");
                        }
                    }
                }
            }else{
                throw new IllegalStateException("Variable achievement data already exist");
            }
        }catch (TransactionException te){
            throw te;
        }
    }

    public void update(Integer id, VarAchievement varAchievement){
        VarAchievement varAchievementOld = varAchievementRepository.findOne(id);
        if(varAchievementOld == null){
            throw new IllegalStateException("No variable achievement data found");
        }
        varAchievement.setId(varAchievementOld.getId());
        varAchievementRepository.save(varAchievementOld);
    }

    public void delete(int id) {
        if (id == 0) {
            return;
        }
        varAchievementRepository.delete(id);
    }

    public VarAchievement findById(int id) {
        if (id == 0) {
            return null;
        }
        return varAchievementRepository.findOne(id);
    }

    public Page<VarAchievement> findAll(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return varAchievementRepository.findAll(pageable);
    }

}
