package kpi.service;

import kpi.entity.Period;
import kpi.repository.PeriodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by chandra on 10/10/14.
 */

@Service
@Transactional
public class PeriodService {

    @Autowired
    private PeriodRepository periodRepository;

    public void save(Period period) {
        periodRepository.save(period);
    }

    public void update(Integer id, Period period){
        Period periodOld = periodRepository.findOne(id);
        if(periodOld == null){
            throw new IllegalStateException("No period data found");
        }
        period.setId(periodOld.getId());
        periodRepository.save(periodOld);
    }

    public void delete(int id) {
        if (id == 0) {
            return;
        }
        periodRepository.delete(id);
    }

    public Period findById(int id) {
        if (id == 0) {
            return null;
        }
        return periodRepository.findOne(id);
    }

    public Page<Period> findAll(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return periodRepository.findAll(pageable);
    }
}
