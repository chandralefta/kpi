package kpi.service;

import kpi.entity.VarType;
import kpi.repository.VarTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by chandra on 10/10/14.
 */

@Service
@Transactional
public class VarTypeService {

    @Autowired
    private VarTypeRepository varTypeRepository;

    public void save(VarType varType) {
        varTypeRepository.save(varType);
    }

    public void update(Integer id, VarType varType){
        VarType varTypeOld = varTypeRepository.findOne(id);
        if(varTypeOld == null){
            throw new IllegalStateException("No variable type data found");
        }
        varType.setId(varTypeOld.getId());
        varTypeRepository.save(varTypeOld);
    }

    public void delete(int id) {
        if (id == 0) {
            return;
        }
        varTypeRepository.delete(id);
    }

    public VarType findVarTypeById(int id) {
        if (id == 0) {
            return null;
        }
        return varTypeRepository.findOne(id);
    }

    public Page<VarType> findAll(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return varTypeRepository.findAll(pageable);
    }
}
