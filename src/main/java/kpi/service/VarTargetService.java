package kpi.service;

import kpi.entity.VarTarget;
import kpi.repository.VarTargetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.util.Date;

/**
 * Created by chandra on 10/10/14.
 */

@Service
@Transactional
public class VarTargetService {

    @Autowired
    private VarTargetRepository varTargetRepository;

    public void save(VarTarget varTarget) {
        java.util.Date now = new java.util.Date();

        java.sql.Date entryDate = new java.sql.Date(now.getTime());

        java.sql.Timestamp validateDate = new java.sql.Timestamp(now.getTime());

        VarTarget varTargetSource = varTargetRepository.findByPeriodMonthAndPeriodYearAndVarId(varTarget.getPeriod().getMonth(), varTarget.getPeriod().getYear(), varTarget.getVar().getId());

        if(varTargetSource != null){
            throw new IllegalStateException("Variable target data already exist");
        }else{
            varTarget.setValidatedDate(validateDate);
            varTarget.setValidatedBy(1);
            varTarget.setEntryDate(entryDate);
            varTarget.setVar(varTarget.getVar());
            varTarget.setTarget(varTarget.getTarget());
            varTarget.setPeriod(varTarget.getPeriod());

            varTargetRepository.save(varTarget);
        }


    }

    public void update(Integer id, VarTarget varVarTarget){
        VarTarget varVarTargetOld = varTargetRepository.findOne(id);
        if(varVarTargetOld == null){
            throw new IllegalStateException("No variable target data found");
        }
        varVarTarget.setId(varVarTargetOld.getId());
        varTargetRepository.save(varVarTargetOld);
    }

    public void delete(int id) {
        if (id == 0) {
            return;
        }
        varTargetRepository.delete(id);
    }

    public VarTarget findById(int id) {
        if (id == 0) {
            return null;
        }
        return varTargetRepository.findOne(id);
    }

    public Page<VarTarget> findAll(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return varTargetRepository.findAll(pageable);
    }
}
