package kpi.service;

import kpi.entity.KpiVar;
import kpi.repository.KpiVarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by chandra on 10/10/14.
 */

@Service
@Transactional
public class KpiVarService {

    @Autowired
    private KpiVarRepository kpiVarRepository;

    public void save(KpiVar kpiVar) {
        kpiVarRepository.save(kpiVar);
    }

    public void update(Integer id, KpiVar kpiVar){
        KpiVar kpiVarOld = kpiVarRepository.findOne(id);
        if(kpiVarOld == null){
            throw new IllegalStateException("No kpi variable found");
        }
        kpiVar.setId(kpiVarOld.getId());
        kpiVarRepository.save(kpiVarOld);
    }

    public void delete(int id) {
        if (id == 0) {
            return;
        }
        kpiVarRepository.delete(id);
    }

    public KpiVar findKpiVarById(int id) {
        if (id == 0) {
            return null;
        }
        return kpiVarRepository.findOne(id);
    }

    public Page<KpiVar> findAll(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return kpiVarRepository.findAll(pageable);
    }
}
