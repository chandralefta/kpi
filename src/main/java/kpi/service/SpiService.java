package kpi.service;

import kpi.entity.*;
import kpi.repository.*;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chandra on 10/8/14.
 */
@Service
//@Transactional
public class SpiService {

    private Session session;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private KpiVarRepository kpiVarRepository;

    @Autowired
    private PeriodRepository periodRepository;

    @Autowired
    private VarTypeRepository varTypeRepository;

    @Autowired
    private VarRepository varRepository;

    @Autowired
    private VarValueRepository varValueRepository;

    public Page<Var> fetchDataNew(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return varRepository.findAll(pageable);
    }

//    public List fetchSpiData(){

//        List<Object> spiData = new ArrayList<Object>();
//
//        List<VarType> varTypes = varTypeRepository.findByKpiVarName("SPI");
//        spiData.add(varTypes);
//
//        for(VarType varType1 : varTypes){
//
//           List<Var> var = varRepository.findOneByVarTypeId(varType1.getId());
//           spiData.add(var);
//
//            for(Var var1 : var){
//                List<VarValue> varValue = (List<VarValue>) varValueRepository.findOneByVarId(var1.getVarType().getId());
//                spiData.add(varValue);
//
//                for(VarValue varValue1 : varValue){
//                    List<Period> period = (List<Period>) periodRepository.findOne(varValue1.getPeriod().getId());
//                    spiData.add(period);
//                }
//            }
//        }

//        List<Object> result = new ArrayList<Object>();
//        try {
//            String querySet = "SELECT kv.*, vt.*, v.*, vv.*, p.*, vr.* "+
//                    "FROM kpi_var AS kv " +
//                    "LEFT JOIN var_type AS vt ON kv.id=vt.kpi_var_id " +
//                    "LEFT JOIN var AS v ON vt.id=v.var_type_id " +
//                    "LEFT JOIN var_value AS vv ON v.id=vv.var_id " +
//                    "LEFT JOIN period AS p ON p.id=vv.period_id "+
//                    "LEFT JOIN var_target AS vr ON v.id=vr.var_id ";
//            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
//            session = sessionFactory.openSession();
//            SQLQuery query = (SQLQuery) session.createSQLQuery(querySet)
//                                                     .addEntity(KpiVar.class)
//                                                     .addEntity(VarType.class)
//                                                     .addEntity(Var.class)
//                                                     .addEntity(VarValue.class)
//                                                     .addEntity(Period.class)
//                                                     .addEntity(VarTarget.class);
//            result = query.list();
//
//        }catch (Exception e){
//            System.out.print(e);
//        }finally{
//            if(session !=null && session.isOpen())
//            {
//                session.close();
//                session=null;
//            }
//        }
//        return result;
//    }

//    @Override
//    public List<Object[]> QueryMultiTable(String querySet) {
//
//        List<Object[]> resultList = new ArrayList<Object[]>();
//
//        try {
//            Query query = entityManager.createNativeQuery(querySet);
//
//           resultList= query.getResultList();
//        }catch (PersistenceException pe){
//            throw pe;
//        }
//
//        return resultList;
//

//
//    }

//    public List<Spi> getByBranchAndPeriode(Integer id, java.util.Date periodeDate){
//        List result = new ArrayList();
//        try {
//            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
//            session = sessionFactory.openSession();
//            SQLQuery query = (SQLQuery) session.createSQLQuery("SELECT s.*, " +
//                                                    "b.*, "+
//                                                    "sg.*, "+
//                                                    "si.*, " +
//                                                    "sr.* "+
//                                                    "FROM spi as s " +
//                                                    "LEFT JOIN branch as b ON b.id=s.branch_id " +
//                                                    "LEFT JOIN segment as sg ON sg.id=s.segment_id " +
//                                                    "LEFT JOIN spi_index as si ON si.id=s.spi_index_id " +
//                                                    "LEFT JOIN spi_realisation as sr ON sr.id=s.spi_realisation_id "+
//                                                    "WHERE s.periode_date >= last_day(:periodeDate) + interval 1 day - interval 3 month " +
//                                                    "AND s.branch_id = :branchId")
//                                                    .addEntity(Spi.class)
//                                                    .addEntity(Branch.class)
//                                                    .addEntity(Segment.class)
//                                                    .addEntity(SpiIndex.class)
//                                                    .addEntity(Achievement.class)
//                                                    .setParameter("branchId", id)
//                                                    .setParameter("periodeDate", periodeDate);
//            result = query.list();
//
//        }catch (Exception e){
//            System.out.print(e);
//        }finally{
//            if(session !=null && session.isOpen())
//            {
//                session.close();
//                session=null;
//            }
//        }
//        return result;
//    }
}