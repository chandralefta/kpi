package kpi.service;

import kpi.configuration.Constants;
import kpi.entity.VarTarget;
import kpi.entity.VarTargetAdj;
import kpi.repository.VarTargetAdjRepository;
import kpi.repository.VarTargetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by chandra on 10/21/14.
 */

@Service
@Transactional
public class VarTargetAdjService {

    @Autowired
    private VarTargetAdjRepository varTargetAdjRepository;

    @Autowired
    private VarTargetRepository varTargetRepository;

    public void save(VarTargetAdj varTargetAdj) {
        //java.util.Date now = new java.util.Date();

        //java.sql.Date entryDate = new java.sql.Date(now.getTime());

        //java.sql.Timestamp validateDate = new java.sql.Timestamp(now.getTime());

        //VarTarget varTargetSource = VarTargetAdjRepository.findByPeriodMonthAndPeriodYearAndVarId(varTarget.getPeriod().getMonth(), varTarget.getPeriod().getYear(), varTarget.getVar().getId());

        VarTarget varTarget = varTargetRepository.findOne(varTargetAdj.getVarTarget().getId());

        float targetAdj = 0.0f;

        targetAdj = varTarget.getTarget() * (Constants.VAR_TARGET_ADJ + varTargetAdj.getAdjustment());

        varTarget.setId(varTargetAdj.getVarTarget().getId());
        varTarget.setTarget(targetAdj);

        varTargetAdj.setPeriod(varTarget.getPeriod());
        varTargetAdj.setVarTarget(varTarget);
        varTargetAdj.setAdjustment(varTargetAdj.getAdjustment());

        varTargetAdjRepository.save(varTargetAdj);

    }

    public void update(Integer id, VarTargetAdj varTargetAdj){
        VarTargetAdj varTargetAdjOld = varTargetAdjRepository.findOne(id);
        if(varTargetAdjOld == null){
            throw new IllegalStateException("No variable target adjustment data found");
        }
        varTargetAdj.setId(varTargetAdjOld.getId());
        varTargetAdjRepository.save(varTargetAdjOld);
    }

    public void delete(int id) {
        if (id == 0) {
            return;
        }
        varTargetAdjRepository.delete(id);
    }

    public VarTargetAdj findById(int id) {
        if (id == 0) {
            return null;
        }
        return varTargetAdjRepository.findOne(id);
    }

    public Page<VarTargetAdj> findAll(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return varTargetAdjRepository.findAll(pageable);
    }
}
