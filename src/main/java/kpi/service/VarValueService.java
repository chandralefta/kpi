package kpi.service;

import kpi.entity.VarAchievement;
import kpi.entity.VarValue;
import kpi.repository.VarValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by chandra on 10/10/14.
 */

@Service
@Transactional
public class VarValueService {

    @Autowired
    private VarValueRepository varValueRepository;

    public void save(VarValue varValue) {
        varValueRepository.save(varValue);
    }

    public void update(Integer id, VarValue varValue){
        VarValue varValueOld = varValueRepository.findOne(id);
        if(varValueOld == null){
            throw new IllegalStateException("No variable value data found");
        }
        varValue.setId(varValueOld.getId());
        varValueRepository.save(varValueOld);
    }

    public void delete(VarValue varValue) {
        if (varValue == null || varValue.getId() == 0) {
            return;
        }
        varValueRepository.delete(varValue);
    }

    public VarValue findBranchById(int id) {
        if (id == 0) {
            return null;
        }
        return varValueRepository.findOne(id);
    }

    public Page<VarValue> findAllBranch(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return varValueRepository.findAll(pageable);
    }

    public VarValue calculateVarValue(VarAchievement varAchievement){
       Float value = 0.0f;
       value += varAchievement.getAchievement();

        VarValue varValue = new VarValue();
        varValue.setVar(varAchievement.getVar());
        varValue.setPeriod(varAchievement.getVarTarget().getPeriod());
        varValue.setValue(value);
       return varValue;
    }


}
