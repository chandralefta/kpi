package kpi.service;

import kpi.configuration.Constants;
import kpi.entity.Product;
import kpi.entity.Var;
import kpi.entity.VarType;
import kpi.repository.ProductRepository;
import kpi.repository.VarRepository;
import kpi.repository.VarTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by chandra on 10/10/14.
 */

@Service
@Transactional
public class VarService {

    @Autowired
    private VarRepository varRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    VarTypeRepository varTypeRepository;

    public void save(Var var) {
        String criteria = "create";
        varRepository.save(calculateVarWeight(var, criteria));
    }

    public void update(Integer id, Var var){
        String criteria = "update";
        Var varOld = varRepository.findOne(id);
        if(varOld == null){
            throw new IllegalStateException("No variable data found");
        }
        var.setId(varOld.getId());
        varRepository.save(calculateVarWeight(var, criteria));
    }

    public void delete(int id) {
        if (id == 0) {
            return;
        }
        varRepository.delete(id);
    }

    public Var findVarById(int id) {
        if (id == 0) {
            return null;
        }
        return varRepository.findOne(id);
    }

    public Page<Var> findAll(Pageable pageable) {
        if(pageable == null){
            pageable = new PageRequest(0, 20);
        }
        return varRepository.findAll(pageable);
    }

    public Var calculateVarWeight(Var var, String criteria){
        float newVarWeight = 0.0f;

        if(criteria.equals("create")) {
            //Var varNew1 = new Var();
            var.setName(var.getName());
            var.setDescription(var.getDescription());


            Product productNew = productRepository.findOne(var.getProduct().getId());
            var.setProduct(productNew);

            VarType varTypeNew = varTypeRepository.findOne(var.getVarType().getId());
            var.setVarType(varTypeNew);


            if (var.getId() == 0 && varTypeNew.getName().equals("NEW")) {
                ;
                newVarWeight = (var.getWeight() * Constants.NEW_WEIGHT) / Constants.PERCENTAGE_DIVIDER;
                var.setWeight(newVarWeight);
            }

            if (var.getId() == 0 && varTypeNew.getName().equals("RO")) {
                ;
                newVarWeight = (var.getWeight() * Constants.RO_WEIGHT) / Constants.PERCENTAGE_DIVIDER;
                var.setWeight(newVarWeight);
            }
        }

        if(criteria.equals("update")){
            Product productNew = productRepository.findOne(var.getProduct().getId());
            var.setProduct(productNew);

            VarType varTypeNew = varTypeRepository.findOne(var.getVarType().getId());
            var.setVarType(varTypeNew);


            if(var.getId() == 0 && varTypeNew.getName().equals("NEW")){;
                newVarWeight = (var.getWeight() * Constants.NEW_WEIGHT)/Constants.PERCENTAGE_DIVIDER;
                var.setWeight(newVarWeight);
            }

            if(var.getId() == 0 && varTypeNew.getName().equals("RO")){;
                newVarWeight = (var.getWeight() * Constants.RO_WEIGHT)/Constants.PERCENTAGE_DIVIDER;
                var.setWeight(newVarWeight);
            }
        }

        return var;

    }
}
