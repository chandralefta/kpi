package kpi.repository;

import kpi.entity.Spi;
import org.hibernate.SessionFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface SpiRepository extends JpaRepository<Spi, Integer>{
}