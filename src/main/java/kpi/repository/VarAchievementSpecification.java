package kpi.repository;

import kpi.entity.*;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

/**
 * Created by chandra on 10/22/14.
 */
public class VarAchievementSpecification {

    public static Specification<VarAchievement> byEntryDateAndTargetId(final java.sql.Date entryDate, final int targetId) {

        return new Specification<VarAchievement>() {
            @Override
            public Predicate toPredicate(final Root<VarAchievement> achievementRoot, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
                Join<VarAchievement, VarTarget> varTargetJoin = achievementRoot.join(VarAchievement_.varTarget);
                return cb.and(cb.equal(achievementRoot.get(VarAchievement_.entryDate), entryDate), cb.equal(varTargetJoin.get(VarTarget_.id), targetId));
            }
        };
    }

    public static Specification<VarAchievement> getMonthly(final int month, final int year, final int targetId) {

        return new Specification<VarAchievement>() {
            @Override
            public Predicate toPredicate(final Root<VarAchievement> achievementRoot, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
                Join<VarAchievement, VarTarget> varTargetJoin = achievementRoot.join(VarAchievement_.varTarget);
                return  cb.and(cb.equal(cb.function("month", Integer.class, achievementRoot.get(VarAchievement_.entryDate)), month), cb.equal(cb.function("year", Integer.class, achievementRoot.get(VarAchievement_.entryDate)), year), cb.equal(varTargetJoin.get(VarTarget_.id), targetId));
            }
        };
    }

    public static Specification<VarAchievement> byVarIdAndTargetIdMonthly(final int varId, final int targetId, final int month, final int year) {

        return new Specification<VarAchievement>() {
            @Override
            public Predicate toPredicate(final Root<VarAchievement> achievementRoot, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
                Join<VarAchievement, VarTarget> varTargetJoin = achievementRoot.join(VarAchievement_.varTarget);
                return  cb.and(cb.equal(cb.function("month", Integer.class, achievementRoot.get(VarAchievement_.entryDate)), month), cb.equal(cb.function("year", Integer.class, achievementRoot.get(VarAchievement_.entryDate)), year), cb.equal(varTargetJoin.get(VarTarget_.id), targetId));
            }
        };
    }

}
