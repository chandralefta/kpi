package kpi.repository;

import kpi.entity.VarValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chandra on 10/10/14.
 */

@Repository
public interface VarValueRepository extends JpaRepository<VarValue, Integer> {

    public List<VarValue> findOneByVarId(int id);
}
