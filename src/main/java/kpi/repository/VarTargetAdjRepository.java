package kpi.repository;

import kpi.entity.VarTargetAdj;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by chandra on 10/21/14.
 */
@Repository
public interface VarTargetAdjRepository extends JpaRepository<VarTargetAdj, Integer> {
}
