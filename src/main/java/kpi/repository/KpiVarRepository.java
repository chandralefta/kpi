package kpi.repository;

import kpi.entity.KpiVar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chandra on 10/10/14.
 */
@Repository
public interface KpiVarRepository extends JpaRepository<KpiVar, Integer> {

    public List<KpiVar> findByName(String name);
}
