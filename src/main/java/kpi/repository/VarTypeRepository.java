package kpi.repository;

import kpi.entity.VarType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chandra on 10/10/14.
 */

@Repository
public interface VarTypeRepository extends JpaRepository<VarType, Integer> {

    public List<VarType> findByKpiVarName(String name);
}
