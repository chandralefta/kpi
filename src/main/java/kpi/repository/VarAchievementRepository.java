package kpi.repository;

import kpi.entity.VarAchievement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by chandra on 10/10/14.
 */

@Repository
public interface VarAchievementRepository extends JpaRepository<VarAchievement, Integer>, JpaSpecificationExecutor<VarAchievement> {
    public VarAchievement findByVarTargetId(Integer varTargetId);
    public VarAchievement findByEntryDateAndVarTargetId(java.sql.Date entryDate, Integer targetId);
}
