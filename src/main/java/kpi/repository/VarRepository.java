package kpi.repository;

import kpi.entity.Var;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chandra on 10/10/14.
 */

@Repository
public interface VarRepository extends JpaRepository<Var, Integer> {

    public List<Var> findOneByVarTypeId(int id);
}
