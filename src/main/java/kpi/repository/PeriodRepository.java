package kpi.repository;

import kpi.entity.Period;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by chandra on 10/10/14.
 */

@Repository
public interface PeriodRepository extends JpaRepository<Period, Integer> {
}
