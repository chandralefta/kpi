package kpi.repository;

import kpi.entity.Branch;
import kpi.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by chandra on 10/10/14.
 */

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
