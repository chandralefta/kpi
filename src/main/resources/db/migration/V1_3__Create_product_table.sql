
CREATE TABLE product
(
	id INTEGER NOT NULL,
	name VARCHAR(50) NOT NULL,
	category_id INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (category_id) REFERENCES category(id)
);
