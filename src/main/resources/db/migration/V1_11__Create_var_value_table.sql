CREATE TABLE var_value
(
	id INTEGER NOT NULL,
    var_id INTEGER NOT NULL,
    period_id INTEGER NOT NULL,
	value FLOAT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (var_id) REFERENCES var(id),
	FOREIGN KEY (period_id) REFERENCES period(id)
);