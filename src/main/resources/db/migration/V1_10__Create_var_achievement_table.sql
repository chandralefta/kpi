CREATE TABLE var_achievement
(
	id INTEGER NOT NULL,
	var_id INTEGER NOT NULL,
    branch_id INTEGER NOT NULL,
    var_target_id INTEGER NOT NULL,
	entry_date DATE NOT NULL,
	achievement FLOAT NOT NULL,
	validated_by INTEGER NOT NULL,
	validated_date TIMESTAMP NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (var_id) REFERENCES var(id),
	FOREIGN KEY (branch_id) REFERENCES branch(id),
	FOREIGN KEY (var_target_id) REFERENCES var_target(id)
);