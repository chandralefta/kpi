CREATE TABLE branch
(
    id INTEGER NOT NULL,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(100),
    PRIMARY KEY(id)
);