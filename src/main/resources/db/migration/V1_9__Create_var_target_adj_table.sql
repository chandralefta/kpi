CREATE TABLE var_target_adj
(
	id INTEGER NOT NULL,
	period_id INTEGER NOT NULL,
    var_target_id INTEGER NOT NULL,
	adjustment FLOAT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (period_id) REFERENCES period(id),
	FOREIGN KEY (var_target_id) REFERENCES var_target(id)
);