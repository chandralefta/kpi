CREATE TABLE var
(
	id INTEGER NOT NULL,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(100) NOT NULL,
	weight FLOAT NOT NULL,
	product_id INTEGER NOT NULL,
	var_type_id INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (product_id) REFERENCES product(id),
	FOREIGN KEY (var_type_id) REFERENCES var_type(id)
);