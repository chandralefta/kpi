CREATE TABLE var_type
(
	id INTEGER NOT NULL,
	name VARCHAR(50) NOT NULL,
	kpi_var_id INTEGER NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (kpi_var_id) REFERENCES kpi_var(id)
);